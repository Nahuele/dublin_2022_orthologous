# Dublin_2022_orthologous



## Objective

Test the use of the reversed version of the sequences to detect orthologous of disordered protein segments, in other words, check if low complexity regions within Intrinsic Disordered Regions (IDR) can impact on the performance of detection of homologous, and detect if its possible to improve this detection using alternative BLAST parameters.

## Files
- found_in_Sc_real_orthologous.csv : real orthologous of _S. cerevisiae_ based on YGOB database. It includes the YGOB accession and the name of the Gene to facilitate mapping.
- homologous_codes.py : Python3 code of the plots and dataframes.
- methodology.odt : contains the step by step protocol.
- orthologous.csv : contains the IDs of orthologous of _E gossypii_ and the two positions of  _S. cerevisiae_.

If you need other files or you have questions send an email to: nahuelescobedo@gmail.com
