from sklearn import preprocessing
import seaborn as sns
import pandas as pd
import os
from matplotlib_venn import venn3, venn3_circles
from matplotlib import pyplot as plt
from Bio import SeqIO
import argparse
import subprocess
import glob
import math

pd.options.display.max_columns = None
pd.options.display.width= None
# pd.options.display.max_rows = None

ruta_tablas = '/home/nahuel/Documentos/Dublin-2022/disordered_homologous/tables/'

uniprot_gossypii = f'{ruta_tablas}UP000000591_284811_gossypii.fasta'
ygob_gossypii = pd.read_csv(f'{ruta_tablas}Egossypii_genome.tab', sep='\t')

uniprot_cerevisiae = f'{ruta_tablas}UP000002311_559292_cerevisiae.fasta'
ygob_cerevisiae = pd.read_csv(f'{ruta_tablas}Scerevisiae_genome.tab', sep='\t', usecols=[0,6])

# columns = ['S. cerevisiae Position 1', 'E. gossypii', 'S. cerevisiae Position 2']
# pillars = pd.read_csv(f'{ruta_tablas}Pillars.tab', sep='\t', usecols= columns)
'''
df2 = pillars[(pillars[columns[0]] == '---') & (pillars[columns[1]] != '---') & (pillars[columns[2]] == '---')].reset_index()

all groups: 570
sc1 vs eg: 3852
sc1 vs sc2: 63
eg vs sc2: 40
sc1 820
sc2 11
eg 484

# venn diagram
# venn3(subsets = (820, 484, 3852, 11, 63, 40, 570), set_labels = columns, alpha = 0.5)
# plt.show()
'''

''' create fasta individual files from proteome big fasta file '''
# parser = argparse.ArgumentParser(description="Split the fasta file into individual file with each gene seq")
# parser.add_argument('-f', action='store', dest='fasta_file', help='Input fasta file')
# result = parser.parse_args()
#
# f_open = open('/home/nahuel/Documentos/Dublin-2022/disordered_homologous/tables/UP000002311_559292_cerevisiae.fasta', "r")
#
# for rec in SeqIO.parse(f_open, "fasta"):
#    id = rec.id
#    description = rec.description
#    id = id.replace('sp|', '').replace('|', '_')
#    seq = rec.seq
#    id_file = open(f'/home/nahuel/Documentos/Dublin-2022/disordered_homologous/tables/individual_cerevisiae_fastas/{id}.seq', "w")
#    id_file.write(">"+str(description)+"\n"+str(seq))
#    id_file.close()
#
# f_open.close()


''' run IUPRED3 '''
# for file in glob.glob(f'{ruta_tablas}individual_cerevisiae_fastas/*', recursive=True):
#     name_file = file.split('/')[-1]
#     print(name_file)
#     iupred = '/home/nahuel/programas/iupred3/iupred3.py'
#     # print(f'python3 {iupred} {file} long > {ruta_tablas}iupred3_output/{name_file}.out')
#     os.system(f'python3 {iupred} {file} long > {ruta_tablas}iupred3_output/{name_file}.out')

''' get IDRs '''
iupred_files = glob.glob(f'{ruta_tablas}iupred3_output/*.seq.out')

''' set your parameters '''
threshold = 0.4
consecutives_res = 50

# for file in iupred_files:
#     sequences = []
#     name = file.split('/')[-1].replace('_YEAST.seq.out', '')
#     with open(file) as f:
#         # read IUpred output file
#         lines_after = f.readlines()[12:]
#         count = 0
#         temp_seq = []
#         for line in lines_after:
#             scores = line.strip().split('\t')
#             res = scores[1]
#             position = scores[0]
#             score = float(scores[2])
#             if score >= threshold:
#                 # temp_seq.append(position + ',') # test position
#                 temp_seq.append(res)
#             else:
#                 sequences.append(temp_seq)
#                 temp_seq = []
#
# # filter low length sequences
#     sequences = set([''.join(x) for x in sequences if len(x) >= consecutives_res])
#     if sequences:
#         for index, seq in enumerate(sequences):
#             print(name)
#             print(index, seq)
#             output_fasta = open(f'{ruta_tablas}IDR_iupred_{str(threshold)}thr_{str(consecutives_res)}len.fasta', "a")
#             output_fasta.write(f'>{str(name)}_{str(index)}\n{str(seq)}\n')
#             output_fasta.close()

''' reverse sequence (Denis Hypothesis) '''
# reversed_out = open(f'{ruta_tablas}IDR_iupred_0.4thr_50len_reversed.fasta', 'w')
# with open(f'{ruta_tablas}IDR_iupred_0.4thr_50len.fasta', 'r') as iupred:
#     for line in iupred.readlines():
#         linea = line.strip()
#         if linea[0] == '>':
#             reversed_out.write(f'{linea}\n')
#         else:
#             seq = linea[::-1]
#             reversed_out.write(f'{seq}\n')

''' I manually add the headers and change the blast output to .csv 
qseqid,sseqid,pident,length,mismatch,gapopen,qstart,qend,sstart,send,evalue,bitscore  
'''

''' process the blastp output
 the path is:
BLAST name (name) -> Sc genes YGOB (name to pillars ID) -> Pillars (pillars ID) 
 '''
# load and clean orthologous dataset (Pillars)
pillars = pd.read_csv('orthologous.csv', sep=';')
pillars["ScerevisiaePositions"] = pillars['ScerevisiaePosition1'].astype(str) + "," + pillars["ScerevisiaePosition2"] # fusion positions 1 and 2 of cerevisiae
pillars.drop(['ScerevisiaePosition1', 'ScerevisiaePosition2'], axis=1, inplace=True)
pillars = pillars[pillars.ScerevisiaePositions != '---,---'] # remove non genes associated rows

# load blast results CHANGE PATH TO YOUR BLAST RESULTS
blastp = pd.read_csv(f'{ruta_tablas}results_iupred_1_fmt.csv', sep=',')
blastp_reversed = pd.read_csv(f'{ruta_tablas}results_IDR_iupred_0.4thr_50len_reversed.csv', sep=',')
blastp[['unip_', 'Name','number']] = blastp['qseqid'].str.split('_', expand=True) # create a Name column to search in Sc genome and then in Pillars the ID

# e values used on the thesis
e_values = [1.0e-08, 1.0e-07, 1.0e-06, 1.0e-05, 0.0001, 0.001, 0.01, 0.1]
p_values = [1.0e-08, 1.0e-07, 1.0e-06, 1.0e-05, 0.0001, 0.001, 0.01, 0.1, 1]
# ratio_values = [1.0e-20, 1.0e-18, 1.0e-16 ,1.0e-14, 1.0e-12, 1.0e-10, 1.0e-08, 1.0e-07, 1.0e-06, 1.0e-05, 0.0001, 0.001, 0.01, 0.1, 1]

# load Sc YGOB genome codes
sc_genes_ygob = pd.read_csv(f"{ruta_tablas}Scerevisiae_genome.tab", sep='\t', usecols= [0, 6], header=None).rename(columns={0: "YGOB_id", 6: "name"})
#         YGOB_id       name
# 0       YAL069W    YAL069W
# 1     YAL068W-A  YAL068W-A
# 2       YAL068C       PAU8
# 3     YAL067W-A  YAL067W-A
# 4       YAL067C       SEO1
print('all genes ygob', sc_genes_ygob.shape[0])
# cleaned orthologous (with no ---)
only_orthologous = pillars[(~pillars['Egossypii'].str.contains('---')) & (pillars['ScerevisiaePositions'] != '---')].reset_index(drop=True)
#            Egossypii           ScerevisiaePositions
# 4452       Agos_2.trna6K     Scer_16.trna7K,Scer_7.trna5K
# 4453       Agos_7.trna19K   Scer_7.trna33K,Scer_10.trna14K
# 4454       Agos_6.trna26T    Scer_14.trna3T,Scer_15.trna2T
# 4455       Agos_5.trna20M        Scer_3.trna6M,---
print('orthologous E. goss / S. cerev', only_orthologous.shape[0])
list_real_orthologous = '|'.join(only_orthologous["ScerevisiaePositions"]).replace(',---', '').replace('---,', '') # STRING
# YAL059W|YBL103C|YDR167W|YDR168W|YDR169C|YDR170C|YDR172W|YDR173C

found_in_Sc = sc_genes_ygob[sc_genes_ygob['YGOB_id'].str.contains(list_real_orthologous)].reset_index(drop=True)

print('orthologous found in YGOB', found_in_Sc.shape[0])
#                   YGOB_id       name
# 16                YAL059W       ECM1
# 17                YAL058W       CNE1
# 20                YAL055W      PEX22

print('---')


# for evalue in e_values:
#     evalues_threshold_below = blastp[blastp['evalue'] <= evalue].reset_index(drop=True)
#
#     # every orth in the pillars that is in blast hits is a True Positive
#     true_pos = found_in_Sc.loc[found_in_Sc['name'].isin(evalues_threshold_below['Name'].tolist())]
#     # every blast hit that is not in the pillars is a False Positive
#     false_pos = evalues_threshold_below.loc[~evalues_threshold_below['Name'].isin(found_in_Sc['name'].tolist())]
#
#     tp = true_pos.shape[0]
#     fp = false_pos.shape[0]
#     print('True positives', tp)
#     print('False positives', fp)
#     print('---')
#     # build DF for plot
#     d = {
#         'e_value': evalue,
#         'True_Positives': tp,
#         'False_Positives':  fp
#     }
#     data_for_plot.append(d)

# data_for_plot = pd.DataFrame(data_for_plot)
# print(data_for_plot)
# y = 'True_Positives'
# x = 'False_Positives'
# p1 = sns.scatterplot(data=data_for_plot, x=x, y=y,  legend=True)
# sns.lineplot(data=data_for_plot, x=x, y=y, estimator='max', color='gray')
# for line in range(0,data_for_plot.shape[0]):
#      p1.text(data_for_plot.False_Positives[line]+0.01, data_for_plot.True_Positives[line],
#      data_for_plot.e_value[line], horizontalalignment='right',
#      size='medium', color='black', weight='normal')

# plt.xscale('log')

''' comparison of basic 2 blast '''
# blastp['dataset'] = 'normal blastp'
# blastp_reversed['dataset'] = 'reversed blastp'
# combined_blast = pd.concat([blastp, blastp_reversed])
# g = sns.displot(combined_blast, x='evalue', hue='dataset',multiple="dodge", bins= 50 )
# print(combined_blast)

''' doing the ratio: lowest regular Pvalue divide by lowest reversed Pvalue'''
lowest_evalue_blast1 = blastp.groupby(['qseqid'])['evalue'].min().reset_index()
lowest_evalue_rev_blast1 = blastp_reversed.groupby(['qseqid'])['evalue'].min().reset_index()

blast_and_reversed = lowest_evalue_blast1.merge(lowest_evalue_rev_blast1, how='left', on='qseqid')
# blast_and_reversed = pd.merge(lowest_evalue_blast1, lowest_evalue_rev_blast1, left_index=True, right_index=True)
blast_and_reversed['Pvalue_regular'] = blast_and_reversed['evalue_x'].apply(lambda x: 1 - math.exp(-x))
blast_and_reversed['Pvalue_reversed'] = blast_and_reversed['evalue_y'].apply(lambda x: 1 - math.exp(-x))
blast_and_reversed['Pvalue_reversed'] = blast_and_reversed['Pvalue_reversed'].fillna(1)

columna = 'ratio_blast_reverse'
blast_and_reversed[columna] =  blast_and_reversed['Pvalue_regular'] / blast_and_reversed['Pvalue_reversed']
blast_and_reversed.sort_values(columna, ascending=False)
blast_and_reversed = blast_and_reversed.astype({columna: float})

''' regular BLAST is -seg no, LOW COMPLEXITY OFF '''
blast_and_reversed['dataset'] = 'low complexity OFF'
blast_and_reversed[['unip_', 'Name','number']] = blast_and_reversed['qseqid'].str.split('_', expand=True)

''' low complexity ON  -seg yes '''
low_comp_blast = pd.read_csv(f'{ruta_tablas}results_filter_low_complex.csv', sep=',')
low_comp_blast_rev = pd.read_csv(f'{ruta_tablas}results_filter_low_complex_reversed.csv', sep=',')
lowest_evalue_blast2 = low_comp_blast.groupby(['qseqid'])['evalue'].min().reset_index() # I only use the minimum (best) e value of blast matches
lowest_evalue_rev_blast2 = low_comp_blast_rev.groupby(['qseqid'])['evalue'].min().reset_index()

# low_comp_blast_and_reversed = pd.merge(lowest_evalue_blast2, lowest_evalue_rev_blast2, left_index=True, right_index=True).reset_index(level=0)
low_comp_blast_and_reversed =  lowest_evalue_blast2.merge(lowest_evalue_rev_blast2, how='left', on='qseqid')
low_comp_blast_and_reversed['Pvalue_regular'] = low_comp_blast_and_reversed['evalue_x'].apply(lambda x: 1 - math.exp(-x))
low_comp_blast_and_reversed['Pvalue_reversed'] = low_comp_blast_and_reversed['evalue_y'].apply(lambda x: 1 - math.exp(-x))
low_comp_blast_and_reversed['Pvalue_reversed'] = low_comp_blast_and_reversed['Pvalue_reversed'].fillna(1)
low_comp_blast_and_reversed[columna] = low_comp_blast_and_reversed['Pvalue_regular'] / low_comp_blast_and_reversed['Pvalue_reversed']
low_comp_blast_and_reversed['dataset'] = 'low complexity ON'
low_comp_blast_and_reversed[['unip_', 'Name','number']] = low_comp_blast_and_reversed['qseqid'].str.split('_', expand=True) # split the column for TP and FP

# sns.lmplot(data=final_to_plot, x="Pvalue_regular", y="Pvalue_reversed", hue='dataset')
# sns.displot(blast_and_reversed2, x=columna, kind="hist", binwidth = 0.1) # binwidth = 0.001
# sns.histplot(data=blast_and_reversed, x=columna, binwidth = 1) # binwidth = 0.001
# sns.kdeplot(low_comp_blast_and_reversed2[columna], shade=True, bw=0.1, alpha=0.1, color='red') # if using seaborn < 0.11.0
# sns.kdeplot(blast_and_reversed[columna], shade=True, bw=0.1, alpha=0.1, color='olive') # if using seaborn < 0.11.0


''' Only P values reshape '''
# blast1= blast_and_reversed[['Pvalue_regular', 'dataset']].assign(blast='normal').rename(columns={"Pvalue_regular": "P_value"})
# blast2= blast_and_reversed[['Pvalue_reversed', 'dataset']].assign(blast='reversed').rename(columns={"Pvalue_reversed": "P_value"})
# blast3= low_comp_blast_and_reversed[['Pvalue_regular', 'dataset']].assign(blast='normal').rename(columns={"Pvalue_regular": "P_value"})
# blast4= low_comp_blast_and_reversed[['Pvalue_reversed', 'dataset']].assign(blast='reversed').rename(columns={"Pvalue_reversed": "P_value"})

# reshape = pd.concat([blast1, blast2, blast3, blast4])
#plotting columns
# sns.displot(reshape, x='P_value', hue=reshape[['dataset', 'blast']].apply(tuple, axis=1), multiple="dodge")

# g = sns.FacetGrid(reshape, col="dataset",  hue="blast")
# g.map(sns.histplot, "P_value", multiple="dodge", shrink=.8)
# g.add_legend()
# ax= sns.displot(final_to_plot, x='evalue_x', multiple="dodge", bins= 50)
# ax= sns.displot(final_to_plot, x='evalue_y', hue='dataset',multiple="dodge", bins= 50)

# plt.yscale('log')
# plt.xscale('log')

# data_for_plot_lc = []
''' after meeting 20 oct , plot FP and TP p values '''
def make_dataF(df_input, values, column, lowCompl):
    temp_list = []

    for pvalue in values:
        pvalues_threshold_below = df_input[df_input[column] <= pvalue].reset_index(drop=True)
        # every orth in the pillars that is in blast hits is a True Positive
        true_pos = found_in_Sc.loc[found_in_Sc['name'].isin(pvalues_threshold_below['Name'].tolist())]
        # every blast hit that is not in the pillars is a False Positive
        false_pos = pvalues_threshold_below.loc[~pvalues_threshold_below['Name'].isin(found_in_Sc['name'].tolist())]

        tp = true_pos.shape[0]
        fp = false_pos.shape[0]
        # build DF for plot
        d = {
            'p_value': pvalue,
            'True_Positives': tp,
            'False_Positives':  fp,
            'Low_complexity': lowCompl
        }
        temp_list.append(d)
    return temp_list


''' normalize Ratio 0 to 1 '''
def normalize(x, col_max):
    if x == -1:
        return np.nan
    else:
        return x/col_max

blast_and_reversed['norm_ratio'] = blast_and_reversed['ratio_blast_reverse'].apply(lambda x: normalize(x, blast_and_reversed['ratio_blast_reverse'].max()))
low_comp_blast_and_reversed['norm_ratio'] = low_comp_blast_and_reversed['ratio_blast_reverse'].apply(lambda x: normalize(x, low_comp_blast_and_reversed['ratio_blast_reverse'].max()))

data_for_plot_ratio = pd.DataFrame(make_dataF(blast_and_reversed, p_values, 'ratio_blast_reverse', 'OFF'))
data_for_plot_ratio_lc = pd.DataFrame(make_dataF(low_comp_blast_and_reversed, p_values, 'ratio_blast_reverse', 'ON'))
data_for_plot = pd.DataFrame(make_dataF(blast_and_reversed, p_values, 'Pvalue_regular', 'OFF'))
data_for_plot_lc = pd.DataFrame(make_dataF(low_comp_blast_and_reversed, p_values, 'Pvalue_regular', 'ON'))
# data_for_plot_rev = pd.DataFrame(make_dataF(blast_and_reversed, p_values, 'Pvalue_reversed', 'OFF'))
# data_for_plot_lc_rev = pd.DataFrame(make_dataF(low_comp_blast_and_reversed, p_values, 'Pvalue_reversed', 'ON'))
# final_to_plot2 = pd.concat([data_for_plot, data_for_plot_lc])

# print(blast_and_reversed[blast_and_reversed.columns[1:-1]])

y = 'True_Positives'
x = 'False_Positives'

# p1 = sns.lineplot(data=final_to_plot2, x=x, y=y,  legend=True, hue='Low_complexity', style='Low_complexity', markers=True)

# plt.plot(data_for_plot_lc_rev[x], data_for_plot_lc_rev[y], linestyle="-", marker="x", label="Low Complx. ON")
# plt.plot(data_for_plot_rev[x], data_for_plot_rev[y], linestyle="-", marker="x", label="Low Complx. OFF")

# p1 = plt.plot(data_for_plot_lc[x], data_for_plot_lc[y], linestyle="-", marker="o", label="Low Complx. ON", alpha=0.5)
# plt.plot(data_for_plot[x], data_for_plot[y], linestyle="-", marker="o", label="Low Complx. OFF", alpha=0.5)
# plt.plot(data_for_plot_ratio_lc[x], data_for_plot_ratio_lc[y], linestyle="-", marker="X", label="Ratio Rev. LC ON", alpha=0.5)
# plt.plot(data_for_plot_ratio[x], data_for_plot_ratio[y], linestyle="-", marker="X", label="Ratio Rev. LC OFF", alpha=0.5)
#
# for line in range(0, data_for_plot.shape[0]):
#      plt.text(data_for_plot[x][line]+0.01, data_for_plot[y][line],
#      data_for_plot['p_value'][line], horizontalalignment='right',
#      size='medium', color='black', weight='normal')

# plt.title(f'P values cutoffs {p_values}')
# plt.xscale('log')
# plt.yscale('log')
# plt.gca().invert_yaxis()
# plt.legend()
# plt.xlabel(x)
# plt.ylabel(y)
# plt.tight_layout()
# plt.show()

''' -log of Pvalues based on name of genes shared or not '''
def dataF_pvaluePlot(df_input, value, column, lowCompl):
    pvalues_threshold_below = df_input[df_input[column] <= value].reset_index(drop=True)
    # every orth in the pillars that is in blast hits is a True Positive
    true_pos = found_in_Sc.loc[found_in_Sc['name'].isin(pvalues_threshold_below['Name'].tolist())].reset_index(drop=True).rename({'name': 'Name'}, axis=1)
    true_pos2 = pd.merge(true_pos, df_input, how="left", on="Name").groupby(['Name'])[column].min().reset_index()
    # every blast hit that is not in the pillars is a False Positive
    false_pos = pvalues_threshold_below.loc[~pvalues_threshold_below['Name'].isin(found_in_Sc['name'].tolist())].reset_index(drop=True).groupby(['Name'])[column].min().reset_index()
    print(false_pos)
    true_pos2['Low_complexity'] = lowCompl
    false_pos['Low_complexity'] = lowCompl

    return true_pos2, false_pos

# trueP, falseP = dataF_pvaluePlot(blast_and_reversed, 10, 'Pvalue_regular', 'OFF')
# trueP_lc, falseP_lc = dataF_pvaluePlot(low_comp_blast_and_reversed, 10, 'Pvalue_regular', 'ON')
# TP_in_both = pd.merge(trueP, trueP_lc, how='inner', left_on = 'Name', right_on = 'Name')
# FP_in_both = pd.merge(falseP, falseP_lc, how='inner', left_on = 'Name', right_on = 'Name')

# plt.scatter(TP_in_both['Pvalue_regular_x'], TP_in_both['Pvalue_regular_y'],  label="True Positives", alpha=0.7)
# plt.scatter(FP_in_both['Pvalue_regular_x'], FP_in_both['Pvalue_regular_y'], color='orange',  label="False Positives", alpha=0.7)

# ratios
trueP, falseP = dataF_pvaluePlot(blast_and_reversed, 10, 'ratio_blast_reverse', 'OFF')
trueP_lc, falseP_lc = dataF_pvaluePlot(low_comp_blast_and_reversed, 10, 'ratio_blast_reverse', 'ON')
# print(trueP_lc, falseP)
TP_in_both_ratio = pd.merge(trueP, trueP_lc, how='inner', left_on = 'Name', right_on = 'Name')
FP_in_both_ratio = pd.merge(falseP, falseP_lc, how='inner', left_on = 'Name', right_on = 'Name')
print(FP_in_both_ratio)
plt.scatter(TP_in_both_ratio['ratio_blast_reverse_x'], TP_in_both_ratio['ratio_blast_reverse_y'],  alpha=0.7)
# plt.scatter(FP_in_both_ratio['ratio_blast_reverse_x'], FP_in_both_ratio['ratio_blast_reverse_y'], color='orange', alpha=0.7)

# sns.scatterplot(data=TP_in_both, x="Pvalue_regular_x", y="Pvalue_regular_y")





plt.title(f'True Positives Ratios')
# plt.xscale('log')
# plt.yscale('log')
# plt.gca().invert_yaxis()
# plt.gca().invert_xaxis()
# plt.legend()
plt.xlabel('ratio Low Compplexity OFF')
plt.ylabel('ratio Low Complexity ON')
plt.tight_layout()
plt.show()
